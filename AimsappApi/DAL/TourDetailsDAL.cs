﻿using AimsappApi.DataManager;
using AimsappApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AimsappApi.DAL
{
    public class TourDetailsDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();

        //get all data from tour master and plan details 
        public List<MasterTourPlan> get_tourDetailsByEmpAndDate(DateTime selectedDate, DateTime endDate,String empId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@employeeId", empId));
                aSqlParameterList.Add(new SqlParameter("@selectedDate", selectedDate));
                aSqlParameterList.Add(new SqlParameter("@endDate", endDate));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetTourPlan_Details_List", aSqlParameterList);
                List<MasterTourPlan> aMasterList = new List<MasterTourPlan>();
                while (dr.Read())
                {
                    MasterTourPlan atour = new MasterTourPlan();
                    atour.TourPlanId = dr["TourPlanID"].ToString();
                    atour.TourPlanDetaisId = dr["TourPlanDetailsID"].ToString();
                    atour.AgentCode= dr["AgentCode"].ToString();
                    atour.IsFarmer = dr["IsFarmer"].ToString();
                    atour.ComDealerCode = dr["ComDealerCode"].ToString();
                    atour.SubDealerCode = dr["SubDealerCode"].ToString();
                    atour.FarmerNo = dr["FarmerNo"].ToString();
                    atour.TourPlanTypeID = dr["TourPlanTypeID"].ToString();
                    atour.TourTrackingNumber = dr["TourTrackingNo"].ToString();
                    atour.DealerOfFarms = dr["DealerOrFarmName"].ToString();
                    atour.VisitingPlace = dr["VisitingPlace"].ToString();
                    atour.TerritoryName = dr["STerritoryName"].ToString();
                    atour.IsVisit = dr["IsVisited"].ToString();
                    atour.PlaceName = dr["Address"].ToString();
                    atour.FarmType = dr["FarmType"].ToString();
                    atour.DealerType = dr["DealerType"].ToString();
                    atour.DistanceCover = dr["Distance"].ToString();
                    atour.Latitude = dr["Latitude"].ToString();
                    atour.Longitude = dr["Longitude"].ToString();
                    atour.TourDate = dr["ToDate"].ToString();

                    atour.FarmerID = dr["FarmerID"].ToString();
                    atour.DealerId = dr["AgentID"].ToString();
                    atour.ComDealerId= dr["ComDealerId"].ToString();
                    atour.SubDealerId = dr["SubDealerId"].ToString();
                    atour.PointLat = dr["PointLat"].ToString();
                    atour.PointLong = dr["PointLong"].ToString();

                    aMasterList.Add(atour);
                }
                return aMasterList;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        //save place lat long address into database table
        public bool SavePlaceDetailsInfo(string tourdetailsId, string address, double latitude, double longitude,double distance)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@TourDetailsID",tourdetailsId));
                aSqlParameterList.Add(new SqlParameter("@Latitude",latitude));
                aSqlParameterList.Add(new SqlParameter("@Longitude",longitude));
                aSqlParameterList.Add(new SqlParameter("@Address", address));
                aSqlParameterList.Add(new SqlParameter("@Distance", distance));

                bool result = accessManager.SaveData("sp_SaveDetailsTo_Place", aSqlParameterList);
                return result;
            }
            catch (SqlException e)
            {
                accessManager.SqlConnectionClose();
                return false;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<MasterTourPlan> get_filterFarmtypes(DateTime selectedDate, DateTime endDate, String empId,String layer, String broiler, String cattle, String sonali, String fish,String colorbird)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@employeeId", empId));
                aSqlParameterList.Add(new SqlParameter("@selectedDate", selectedDate));
                aSqlParameterList.Add(new SqlParameter("@endDate", endDate));
                aSqlParameterList.Add(new SqlParameter("@layer", layer));
                aSqlParameterList.Add(new SqlParameter("@broiler", broiler));
                aSqlParameterList.Add(new SqlParameter("@cattle", cattle));
                aSqlParameterList.Add(new SqlParameter("@sonali", sonali));
                aSqlParameterList.Add(new SqlParameter("@fish", fish));
                aSqlParameterList.Add(new SqlParameter("@colorbird", colorbird));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetFiterFarmtypeList", aSqlParameterList);
                List<MasterTourPlan> aMasterList = new List<MasterTourPlan>();
                while (dr.Read())
                {
                    MasterTourPlan atour = new MasterTourPlan();
                    atour.TourPlanId = dr["TourPlanID"].ToString();
                    atour.TourPlanDetaisId = dr["TourPlanDetailsID"].ToString();
                    atour.AgentCode = dr["AgentCode"].ToString();
                    atour.IsFarmer = dr["IsFarmer"].ToString();
                    atour.ComDealerCode = dr["SubDealerCode"].ToString();
                    atour.SubDealerCode = dr["ComDealerCode"].ToString();
                    atour.FarmerNo = dr["FarmerNo"].ToString();
                    atour.TourPlanTypeID = dr["TourPlanTypeID"].ToString();
                    atour.TourTrackingNumber = dr["TourTrackingNo"].ToString();
                    atour.DealerOfFarms = dr["DealerOrFarmName"].ToString();
                    atour.VisitingPlace = dr["VisitingPlace"].ToString();
                    atour.TerritoryName = dr["STerritoryName"].ToString();
                    atour.IsVisit = dr["IsVisited"].ToString();
                    atour.PlaceName = dr["Address"].ToString();
                    atour.FarmType = dr["FarmType"].ToString();
                    atour.DealerType = dr["DealerType"].ToString();
                    atour.DistanceCover = dr["Distance"].ToString();
                    atour.Latitude = dr["Latitude"].ToString();
                    atour.Longitude = dr["Longitude"].ToString();
                    atour.TourDate = dr["ToDate"].ToString();
                    aMasterList.Add(atour);
                }
                return aMasterList;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}