﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;
using AimsappApi.DataManager;
using AimsappApi.Models;

namespace AimsappApi.DAL
{
    public class EmployeeDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();


        public List<AttendanceReport> _getEmployeeAttendance(DateTime fromDate, DateTime toDate,string empid)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.HumanResourceDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empIdN", empid));
                aSqlParameterList.Add(new SqlParameter("@fromDate", fromDate));
                aSqlParameterList.Add(new SqlParameter("@toDate", toDate));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetAttendanceDetails_AIMSAPI", aSqlParameterList);
                
             
                List<AttendanceReport> aMasterList = new List<AttendanceReport>();

                while (dr.Read())
                {
                    AttendanceReport aInfo = new AttendanceReport();

                    if (dr["EmpNo"] != DBNull.Value)
                    {
                        aInfo.EmpNo = Convert.ToInt32(dr["EmpNo"]);
                    }
                    if (dr["EMPID"] != DBNull.Value)
                    {
                        aInfo.EMPID = dr["EMPID"].ToString();
                    }


                    if (dr["ShiftEnd"] != DBNull.Value)
                    {
                        aInfo.ShiftEnd = dr["ShiftEnd"].ToString();

                    }


                    if (dr["ShiftStart"] != DBNull.Value)
                    {
                        aInfo.ShiftStart = dr["ShiftStart"].ToString();


                    }

                    aInfo.AttDate = dr["AttDate"].ToString();
                    aInfo.ActualAttDate = dr["ActualAttDate"].ToString();
                    aInfo.DayName = dr["DayName"].ToString();
                    aInfo.InTime = dr["InTime"].ToString();
                    aInfo.OutTime = dr["OutTime"].ToString();
                    aInfo.Status = dr["ATTStatus"].ToString();
                    aInfo.DutyDuration = dr["DutyDuration"].ToString();
                    aInfo.Remarks = dr["Remarks"].ToString();
                    aMasterList.Add(aInfo);
   
                }

                return aMasterList;

            }
            catch (Exception e)
            {
                throw e;

            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }




        public List<CalenderModel> GetCalendarData()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.HumanResourceDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCalendar_AIMSAPI");
                List<CalenderModel> aList = new List<CalenderModel>();

                while (dr.Read())
                {
                    CalenderModel aInfo = new CalenderModel();
                    aInfo.CalendarId = Convert.ToInt16(dr["CalendarId"]);
                    aInfo.cDate = dr["cDate"].ToString();
                    aInfo.IsCurrentMonth = Convert.ToBoolean(dr["IsCurrentMonth"]);

                    aList.Add(aInfo);
                }
                return aList;
            }
            catch (Exception exception)
            {
                List<CalenderModel> aListError = new List<CalenderModel>();

                CalenderModel aInfo = new CalenderModel();
                aInfo.IsError = true;
                aInfo.ErrorMessage = exception.Message.ToString();
                aListError.Add(aInfo);
                return aListError;
                
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public PaySlipModel GetPaySlipData(int calendar, string user)
        {
            PaySlipModel aInfo = new PaySlipModel();

            try
            {
                accessManager.SqlConnectionOpen(DataBase.HumanResourceDB);
                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                gSqlParameterList.Add(new SqlParameter("@CalendarID", calendar));
                gSqlParameterList.Add(new SqlParameter("@EmpID", user));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetPayslipAdmin_AIMSAPI", gSqlParameterList);
                while (dr.Read())
                {

                    aInfo.CalenderId = Convert.ToInt32(dr["CalendarId"]);
                    aInfo.TotalAbsent = Convert.ToInt16(dr["TotalAbsent"]);
                    aInfo.Absenteeism = Convert.ToInt32(dr["Absenteeism"]);
                    aInfo.Gross = Convert.ToInt32(dr["Gross"]);
                    aInfo.ActualGross = Convert.ToInt32(dr["ActualGross"]);
                    aInfo.Basic = Convert.ToInt32(dr["Basic"]);
                    aInfo.HouseRent = Convert.ToInt32(dr["HouseRent"]);
                    aInfo.Medical = Convert.ToInt32(dr["Medical"]);
                    aInfo.MobileAllowance = Convert.ToInt32(dr["MobileAllowance"]);
                    aInfo.OtherAllowance = Convert.ToInt32(dr["OtherAllowance"]);
                    aInfo.Tax = Convert.ToInt32(dr["Tax"]);
                    aInfo.TaxDeductionFromEmp = Convert.ToInt32(dr["TaxDeductionFromEmp"]);
                    aInfo.TaxAdjustFromCompany = Convert.ToInt32(dr["TaxAdjustFromCompany"]);
                    aInfo.OtherDeduction = Convert.ToInt32(dr["OtherDeduction"]);
                    aInfo.NetPayable = Convert.ToInt32(dr["NetPayable"]);
                    aInfo.ConveyanceAllowance = Convert.ToInt32(dr["ConveyanceAllowance"]);
                    aInfo.SpecialAllowance = Convert.ToInt32(dr["SpecialAllowance"]);
                    aInfo.FoodCharge = Convert.ToInt32(dr["FoodCharge"]);
                    aInfo.TiffinAllowance = Convert.ToInt32(dr["TiffinAllowance"]);
                    aInfo.Arrear = Convert.ToInt32(dr["Arrear"]);
                    aInfo.AdvancedSalaryDeduction = Convert.ToInt32(dr["AdvancedSalaryDeduction"]);
                    aInfo.MotorCycleLoadDeduction = Convert.ToInt32(dr["MotorCycleLoadDeduction"]);
                    aInfo.ProvidentfundDeduction = Convert.ToInt32(dr["ProvidentfundDeduction"]);
                    aInfo.ProvidentFundFromCompany = Convert.ToInt32(dr["ProvidentFundFromCompany"]);
                    aInfo.PFLoanDeduction = Convert.ToInt32(dr["PFLoanDeduction"]);
                    aInfo.TransportAllowance = Convert.ToInt32(dr["TransportAllowance"]);
                    aInfo.MobileBillDeduction = Convert.ToInt32(dr["MobileBillDeduction"]);
                    aInfo.HardshipAllowance = Convert.ToInt32(dr["HardshipAllowance"]);
                    aInfo.OtherEarningAllowance = Convert.ToInt32(dr["OtherEarningAllowance"]);
                    aInfo.FarmAllowance = Convert.ToInt32(dr["FarmAllowance"]);
                    aInfo.TechnicalAllowance = Convert.ToInt32(dr["TechnicalAllowance"]);
                    aInfo.UtilityDeduction = Convert.ToInt32(dr["UtilityDeduction"]);
                    aInfo.AccommodationAllowance = Convert.ToInt32(dr["AccommodationAllowance"]);
                    aInfo.ConsultedAmount = Convert.ToInt32(dr["ConsultedAmount"]);
                    aInfo.FoodAllowance = Convert.ToInt32(dr["FoodAllowance"]);
                    aInfo.AdjustmentDeduction = Convert.ToInt32(dr["AdjustmentDeduction"]);
                    aInfo.HouseAllowanceDeduction = Convert.ToInt32(dr["HouseAllowanceDeduction"]);
                    aInfo.SpecialBonus = Convert.ToInt32(dr["SpecialBonus"]);

                    aInfo.EmpName = dr["EmpName"].ToString();
                    aInfo.EMPID = dr["EMPID"].ToString();
                    aInfo.JobLocationName = dr["JobLocationName"].ToString();
                    aInfo.Designation = dr["Designation"].ToString();
                    aInfo.PaySlipMonth = dr["PaySlipBank"].ToString();
                    aInfo.YearNo = dr["YearNo"].ToString();





                }
                return aInfo;
            }
            catch (Exception exception)
            {
                aInfo.IsError = true;
                aInfo.ErrorMessage = exception.Message.ToString();
                return aInfo;

            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



        public EmpLeaveDetails GetEmpLeaveDetails(int emp)
        {
            EmpLeaveDetails aInfo = new EmpLeaveDetails();

            try
            {
          
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aParameterList = new List<SqlParameter>();
                aParameterList.Add(new SqlParameter("@EmpNo", emp));         
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetLeaveCalDetails_AIMSAPP", aParameterList);
                while (dr.Read())
                {

                    aInfo.EmpNo = (int)dr["EmpNo"];
                    aInfo.Gender = dr["Gender"].ToString();
                    aInfo.TCausal = dr["TCausal"].ToString();
                    aInfo.TMedical = dr["TMedical"].ToString();
                    aInfo.TAnnual = dr["TAnnual"].ToString();
                    aInfo.ECausal = dr["ECausal"].ToString();
                    aInfo.EMedical = dr["EMedical"].ToString();
                    aInfo.EAnnual = dr["EAnnual"].ToString();
                    aInfo.BCausal = dr["BCausal"].ToString();
                    aInfo.BMedical = dr["BMedical"].ToString();
                    aInfo.BAnnual = dr["BAnnual"].ToString();
                    aInfo.ACausal = dr["ACausal"].ToString();
                    aInfo.AMedical = dr["AMedical"].ToString();
                    aInfo.AAnnual = dr["AAnnual"].ToString();
                    aInfo.LWP = dr["LWP"].ToString();
                    aInfo.Maternity = dr["Maternity"].ToString();

                    aInfo.FunctionReportingName = dr["FunctionReportingName"].ToString();
                    aInfo.FunctionReportingID = dr["FunctionReportingID"].ToString();
                    aInfo.AdminReportingName = dr["AdminReportingName"].ToString();
                    aInfo.AdminReportingID = dr["AdminReportingID"].ToString();

                }

                return aInfo;

            }
            catch (Exception ex)
            {
           
                aInfo.IsError = true;
                aInfo.ErrorMessage = ex.Message.ToString();

                return aInfo;
              
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }




        public EmployeeInfo _getEmployeeInfoByEmpNoSelectedInfo(int empno)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.HumanResourceDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empno", empno));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetEmpinfoByEmpNo_AIMSAPI", aSqlParameterList);
                EmployeeInfo aInfo = new EmployeeInfo();
                while (dr.Read())
                {
                    aInfo.EmpNo = Convert.ToInt32(dr["EmpNo"]);
                    aInfo.EMPID = dr["EMPID"].ToString();
                    aInfo.EmpName = dr["EmpName"].ToString();
                    aInfo.Gender = dr["Gender"].ToString();
                    aInfo.Designation = dr["Designation"].ToString();
                    aInfo.JoiningDate = dr["JoiningDate"].ToString();
                    aInfo.GradeName = dr["GradeName"].ToString();
                    aInfo.DepartmentName = dr["DepartmentName"].ToString();
                    aInfo.JobLocation = dr["JobLocationName"].ToString();
                    aInfo.SalaryLocation = dr["SalaryLocation"].ToString();
                    aInfo.EmpType = dr["EmpType"].ToString();
                    aInfo.CompanyName = dr["CompanyName"].ToString();
                    aInfo.OfficeEmail = dr["OfficeEmail"].ToString();
                    aInfo.PersonalMobile = dr["PersonalMobileNo"].ToString();
                    aInfo.OfficeMobile = dr["OfficeMobileNo"].ToString();
                    aInfo.Address = dr["PresentAddress"].ToString();
                    aInfo.BloodGroup = dr["BloodGroupName"].ToString();
                }

                return aInfo;

            }
            catch (Exception e)
            {
                throw e;

            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }






        public LeaveInformation SaveLeave(LeaveInformation aMaster)
        {
            LeaveInformation aInformation = new LeaveInformation();

            bool result = false;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                gSqlParameterList.Add(new SqlParameter("@EmpNo", aMaster.EmpNo));
                gSqlParameterList.Add(new SqlParameter("@FromDate", aMaster.FromDate));
                gSqlParameterList.Add(new SqlParameter("@ToDate", aMaster.ToDate));
                gSqlParameterList.Add(new SqlParameter("@IsShortLeave", aMaster.IsShortLeave));
                gSqlParameterList.Add(new SqlParameter("@LeaveTypeId", aMaster.LeaveTypeId));
                gSqlParameterList.Add(new SqlParameter("@Purpose", aMaster.Purpose));
                gSqlParameterList.Add(new SqlParameter("@RequestTo", aMaster.RequestTo));
                gSqlParameterList.Add(new SqlParameter("@AddressDuringLeave", aMaster.AddressDuringLeave));
                gSqlParameterList.Add(new SqlParameter("@AdminAuth", aMaster.AdminAuth));
                gSqlParameterList.Add(new SqlParameter("@ResponsiblePersonDuringLeave", aMaster.ResponsiblePersonDuringLeave));
                gSqlParameterList.Add(new SqlParameter("@EntryDate", DateTime.Now));

                int pk = accessManager.SaveDataReturnPrimaryKey("sp_AddLeaveFromAdmin_AIMSAPI", gSqlParameterList);
                if (pk > 0)
                {
                    aInformation.IsSuccess = true;




                    LeaveApplyNotifyReqInfo apply = SendNoteficationLeaveApply(pk);


                    if (apply != null && apply.IsError == false && IsValidEmail(apply.OfficeMail))
                    {
                        MailMessage aMail = new MailMessage();
                        string body = "(This is a System Generated Message)<br>Dear Sir,";
                        body += "<p style='font-size:12px ;' ><br> A Leave has been applied by " + apply.EMPID + " - " + apply.EmpName + " Designation: " + apply.Designation + " from AIMS APP";
                        body += "<br> Please Login to <a href='http://aims.info.bd:90/' target='_blank'> Personnel Portal</a> to Approve";
                        aMail.Body = body;
                        aMail.Subject = "Personnel Portal";

                        // MailAddressCollection aAddress = new MailAddressCollection(mail.Email);
                        if (!string.IsNullOrEmpty(apply.OfficeMail))
                        {
                            aMail.To.Add(new MailAddress(apply.OfficeMail));
                        }
                        if (aMail.To.Count > 0)
                        {
                            Thread emailT = new Thread(delegate ()
                            {

                                MailConfigDetails.MailSetUpHr(aMail);


                            });
                            emailT.IsBackground = true;
                            emailT.Start();
                        }

                        if (!string.IsNullOrEmpty(apply.OfficeMobile))
                        {
                            SendSmsLeaveApply(apply);
                        }

                    }
                }

                
              

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                aInformation.IsError = true;
                aInformation.ErrorMessage = exception.Message;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return aInformation;
        }



        public LeaveInformation UpdateLeave(LeaveInformation aMaster)
        {
            LeaveInformation aInformation = new LeaveInformation();

            bool result = false;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                gSqlParameterList.Add(new SqlParameter("@leaveId", aMaster.LeaveId));
                gSqlParameterList.Add(new SqlParameter("@EmpNo", aMaster.EmpNo));
                gSqlParameterList.Add(new SqlParameter("@FromDate", aMaster.FromDate));
                gSqlParameterList.Add(new SqlParameter("@ToDate", aMaster.ToDate));
                gSqlParameterList.Add(new SqlParameter("@IsShortLeave", aMaster.IsShortLeave));
                gSqlParameterList.Add(new SqlParameter("@LeaveTypeId", aMaster.LeaveTypeId));
                gSqlParameterList.Add(new SqlParameter("@Purpose", aMaster.Purpose));
                gSqlParameterList.Add(new SqlParameter("@RequestTo", aMaster.RequestTo));
                gSqlParameterList.Add(new SqlParameter("@AddressDuringLeave", aMaster.AddressDuringLeave));
                gSqlParameterList.Add(new SqlParameter("@AdminAuth", aMaster.AdminAuth));
                gSqlParameterList.Add(new SqlParameter("@ResponsiblePersonDuringLeave", aMaster.ResponsiblePersonDuringLeave));
                gSqlParameterList.Add(new SqlParameter("@EntryDate", DateTime.Now));

                aInformation.IsSuccess = accessManager.UpdateData("sp_UpdateLeaveFromAdmin_AIMSAPI", gSqlParameterList);

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                aInformation.IsError = true;
                aInformation.ErrorMessage = exception.Message;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return aInformation;
        }


        public LeaveApplyNotifyReqInfo SendNoteficationLeaveApply(int leaveId)
        {
            DataAccessManager dAc = new DataAccessManager();
            LeaveApplyNotifyReqInfo apply = new LeaveApplyNotifyReqInfo();
            try
            {
     
            


        List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@leaveId", leaveId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_LeaveApplyRequestInfo_AIMSAPI", aParameters);
        

                while (dr.Read())
                {
                    apply.Designation = dr["Designation"].ToString();
                    apply.OfficeMail = dr["OfficeMail"].ToString();
                    apply.OfficeMobile = dr["OfficeMobile"].ToString();
                    apply.EmpName = dr["EmpName"].ToString();
                    apply.EMPID = dr["EMPID"].ToString();
                    apply.Designation = dr["Designation"].ToString();

                }



                return apply;
            }
            catch (Exception exception)
            {
                apply.IsError = true;
                return apply;

            }

      

        }

        public void SendSmsLeaveApply(LeaveApplyNotifyReqInfo apply)
        {
            string MessageBody = "";
            MessageBody = "A Leave has been applied by " + apply.EmpName + " (" + apply.EMPID + ")-" + apply.Designation + " from AIMS APP" + ". Please, Login To Personnel Portal Software To Approve";
            string sid = "ABFL";
            string user = "abfl";
            string pass = "abfl@123";
            string URI = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
            string myParameters = "user=" + user + "&pass=" + pass + "&sms[0][0]=" + apply.OfficeMobile + "&sms[0][1]=" + System.Web.HttpUtility.UrlEncode(MessageBody) + "&sms[0][2]=123456789&sid=" + sid;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
            byte[] data = Encoding.ASCII.GetBytes(myParameters);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            using (Stream stream = request.GetRequestStream()) { stream.Write(data, 0, data.Length); }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            string ErrorId = "0";
            if (!string.IsNullOrEmpty(responseString))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseString);
                XmlNodeList ErrorIdTags = xmlDoc.GetElementsByTagName("REFERENCEID");
                if (ErrorIdTags.Count < 1)
                {
                    ErrorId = "0";
                }
                else
                {
                    // The tag could be found!
                    ErrorId = ErrorIdTags[0].InnerText;
                }
            }

        }




        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }



    }
}