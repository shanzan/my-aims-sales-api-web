﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AimsappApi.DataManager;
using AimsappApi.Models;

namespace AimsappApi.DAL
{
    public class AppUtilityDAL
    {
        private DataAccessManager _accessManager = new DataAccessManager();


        public AppVersionModal _getAppVersionInfo()
        {
            AppVersionModal aInfo = new AppVersionModal();
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetSalesAppVersionInfo");
                while (dr.Read())
                {
                    aInfo.AppVersionID = Convert.ToInt32(dr["AppVersionID"]);
                    aInfo.AppVersionNumber = Convert.ToInt32(dr["AppVersionNumber"]);
                    aInfo.AppVersionName = dr["AppVersionName"].ToString();
                }
            }
            catch (Exception e)
            {
                aInfo.ErrorStatus = true;
                aInfo.ErrorMessage = e.ToString();

            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return aInfo;

        }
    }
}