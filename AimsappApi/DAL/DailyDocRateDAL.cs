﻿using AimsappApi.DataManager;
using AimsappApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AimsappApi.DAL
{
    public class DailyDocRateDAL
    {
        private DataAccessManager _accessManager = new DataAccessManager();

        public List<DailyDocRate> _getDailyDocRate(DateTime rateOf, string userId)
        {
            List<DailyDocRate> aList = new List<DailyDocRate>();
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empid", userId));
                aSqlParameterList.Add(new SqlParameter("@rateOf", rateOf));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDailyDOCRateApp", aSqlParameterList);

                while (dr.Read())
                {
                    DailyDocRate aInformation = new DailyDocRate();
                    aInformation.RateDate = dr["RateDate"].ToString();
                    aInformation.SAreaName = dr["SAreaName"].ToString();
                    aInformation.Nwm = dr["Nwm"].ToString();
                    aInformation.Nwf = dr["Nwf"].ToString();
                    aInformation.Nbm = dr["Nbm"].ToString();
                    aInformation.Nbf = dr["Nbf"].ToString();
                    aInformation.Hc = dr["Hc"].ToString();
                    aInformation.HbColor = dr["hColor"].ToString();
                    aInformation.Ir = dr["Ir"].ToString();
                    aInformation.RevisionNo = dr["RevisionNo"].ToString();
                    aInformation.RevisionTime = dr["RevisionTime"].ToString();
                    aInformation.ErrorStatus = false;
                    aInformation.ErrorMessage = "Success";
                    aList.Add(aInformation);
                }
                return aList;
            }
            catch (Exception e)
            {
                DailyDocRate aInformation = new DailyDocRate();
                aInformation.ErrorStatus = true;
                aInformation.ErrorMessage = e.ToString();
                aList.Add(aInformation);
                return aList;

            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}