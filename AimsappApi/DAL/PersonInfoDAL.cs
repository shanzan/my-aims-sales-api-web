﻿using AimsappApi.DataManager;
using AimsappApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AimsappApi.DAL
{
    public class PersonInfoDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();

        //get dealer information 
        public List<PersonInfoModel> get_DealerInformation(string empID)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empID", empID));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetDealerInformation",aSqlParameterList);
                List<PersonInfoModel> aPersonInfolist = new List<PersonInfoModel>();
                while (dr.Read())
                {
                    PersonInfoModel pInfo = new PersonInfoModel();
                    pInfo.PersonId = dr["PersonID"].ToString();
                    pInfo.PersonCode= dr["PersonCode"].ToString();
                    pInfo.PersonName = dr["PersonName"].ToString();
                    pInfo.PersonAddress = dr["PersonAddress"].ToString();
                    pInfo.PersonMobileNo = dr["PersonMobile"].ToString();
                    pInfo.PersonTypeID = dr["PersonType"].ToString();
                    pInfo.PersonFamType = "NONE";
                    pInfo.PersonApproved= dr["PersonApproved"].ToString();
                    pInfo.PersonLat= dr["PointLat"].ToString();
                    pInfo.PersonLong = dr["PointLang"].ToString();
                    aPersonInfolist.Add(pInfo);
                }
                return aPersonInfolist;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<PersonInfoModel> get_FarmerInformation(string empID)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empID", empID));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetFarmerInformation",aSqlParameterList);
                List<PersonInfoModel> aPersonInfolist = new List<PersonInfoModel>();
                while (dr.Read())
                {
                    PersonInfoModel pInfo = new PersonInfoModel();
                    pInfo.PersonId = dr["PersonID"].ToString();
                    pInfo.PersonCode = dr["PersonCode"].ToString();
                    pInfo.PersonName = dr["PersonName"].ToString();
                    pInfo.PersonAddress = dr["PersonAddress"].ToString();
                    pInfo.PersonMobileNo = dr["PersonMobile"].ToString();
                    pInfo.PersonTypeID = dr["PersonType"].ToString();
                    pInfo.PersonFamType = "NONE";
                    pInfo.PersonApproved = dr["PersonApproved"].ToString();
                    pInfo.PersonLat = dr["PointLat"].ToString();
                    pInfo.PersonLong = dr["PointLang"].ToString();
                    aPersonInfolist.Add(pInfo);
                }
                return aPersonInfolist;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<PersonInfoModel> get_subDealerInformation(string empID)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empID", empID));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Subdealer_Information",aSqlParameterList);
                List<PersonInfoModel> aPersonInfolist = new List<PersonInfoModel>();
                while (dr.Read())
                {
                    PersonInfoModel pInfo = new PersonInfoModel();
                    pInfo.PersonId = dr["PersonID"].ToString();
                    pInfo.PersonCode = dr["PersonCode"].ToString();
                    pInfo.PersonName = dr["PersonName"].ToString();
                    pInfo.PersonAddress = dr["PersonAddress"].ToString();
                    pInfo.PersonMobileNo = dr["PersonMobile"].ToString();
                    pInfo.PersonTypeID = dr["PersonType"].ToString();
                    pInfo.PersonFamType = "NONE";
                    pInfo.PersonApproved = dr["PersonApproved"].ToString();
                    pInfo.PersonLat = dr["PointLat"].ToString();
                    pInfo.PersonLong = dr["PointLang"].ToString();
                    aPersonInfolist.Add(pInfo);
                }
                return aPersonInfolist;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public bool SaveLocationInformation(string personId, string personCode, int personType,double platitude, double plongitude, string pFarmtype,string empID)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@PointId",personId));
                aSqlParameterList.Add(new SqlParameter("@PointCode", personCode));
                aSqlParameterList.Add(new SqlParameter("@PointType", personType));
                aSqlParameterList.Add(new SqlParameter("@PointLat", platitude));
                aSqlParameterList.Add(new SqlParameter("@PointLong", plongitude));
                aSqlParameterList.Add(new SqlParameter("@PointFarmType", pFarmtype));
                aSqlParameterList.Add(new SqlParameter("@empID", empID));
                bool result = accessManager.SaveData("sp_SaveLocationTemporary", aSqlParameterList);
                return result;
            }
            catch (SqlException e)
            {
                accessManager.SqlConnectionClose();
                return false;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}