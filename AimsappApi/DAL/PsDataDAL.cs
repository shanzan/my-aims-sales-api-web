﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AimsappApi.DataManager;
using AimsappApi.Models;

namespace AimsappApi.DAL
{
    public class PsDataDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();

        public List<PsProductionData> _getPsProductionData(DateTime productionDate, string typeB,string empId)
        {
            List<PsProductionData> aInfo = new List<PsProductionData>();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@productionDate", productionDate));
                aSqlParameterList.Add(new SqlParameter("@typeB", typeB));
                aSqlParameterList.Add(new SqlParameter("@empId", empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetPsProductionData", aSqlParameterList);
                while (dr.Read())
                {
                    PsProductionData aInformation = new PsProductionData();
                    aInformation.ProductionDate = dr["ProductionDate"].ToString();
                    aInformation.FilterName = dr["FilterName"].ToString();
                    aInformation.House = dr["House"].ToString();
                    aInformation.FlockAge = dr["FlockAge"].ToString();
                    aInformation.Mm = dr["Mm"].ToString();
                    aInformation.Mc = dr["Mc"].ToString();
                    aInformation.Mk = dr["Mk"].ToString();
                    aInformation.Fm = dr["Fm"].ToString();
                    aInformation.Fc = dr["Fc"].ToString();
                    aInformation.Fk = dr["Fk"].ToString();
                    aInformation.ProdStd = dr["ProdStd"].ToString();
                    aInformation.ActPercent = dr["ActPercent"].ToString();
                    aInformation.Pe = dr["Pe"].ToString();
                    aInformation.He = dr["He"].ToString();
                    aInformation.Hhp = dr["hhp"].ToString();
                    aInformation.Type = dr["Type"].ToString();
                    aInformation.FlockNo = dr["FlockNo"].ToString();
                    aInfo.Add(aInformation);

                }


                return aInfo;
            }
            catch (Exception e)
            {
                PsProductionData aInformation = new PsProductionData();
                aInformation.ErrorStatus = true;
                aInformation.ErrorMessage= e.Message;


                aInfo.Add(aInformation);
                return aInfo;

            }


            finally
            {
                accessManager.SqlConnectionClose();
            }
        }





        public List<BreederInfo> _getBreederActiveInfo()
        {
            List<BreederInfo> aInfo = new List<BreederInfo>();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetBreederInfoAllActive", aSqlParameterList);

                while (dr.Read())
                {
                    BreederInfo aInformation = new BreederInfo();
                    aInformation.BreederName = dr["BreederName"].ToString();
                    aInformation.BreederShortName = dr["BreederShortName"].ToString();
                    aInfo.Add(aInformation);

                }

                return aInfo;
            }
            catch (Exception e)
            {
                BreederInfo aInformation = new BreederInfo();
                aInformation.ErrorStatus = true;
                aInformation.ErrorMessage = e.Message;


                aInfo.Add(aInformation);
                return aInfo;

            }


            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}