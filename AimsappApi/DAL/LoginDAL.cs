﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using AimsappApi.DataManager;
using AimsappApi.Models;

namespace AimsappApi.DAL
{
    public class LoginDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();


        public Login LoginCheck(string username, string password)
        {
            Login UserInfoList = new Login();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);
          

                string passwordEncrpt = EncryptDecrypt.EncryptText(password);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@username", username));
                aSqlParameterList.Add(new SqlParameter("@password", passwordEncrpt));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckLogin_AIMSAPI", aSqlParameterList);
                while (dr.Read())
                {
                    UserInfoList.EMPID = dr["LoginUserEmpId"].ToString();
                    UserInfoList.EmpName = dr["EmpName"].ToString();
                    UserInfoList.EmpNo = Convert.ToInt32(dr["EmpNo"].ToString());
                    UserInfoList.AppVersionName = dr["AppVersionName"].ToString();
                }
         
            }
            catch (Exception exception)
            {
                UserInfoList.ErrorStatus = true;
                UserInfoList.ErrorMessage = exception.ToString();


            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return UserInfoList;
        }



        public EmployeeInfo _getEmployeeInfoByEmpNo(int empno)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.HumanResourceDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empno", empno));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetEmpinfoByEmpNo_AIMSAPI", aSqlParameterList);
                EmployeeInfo aInfo = new EmployeeInfo();
                while (dr.Read())
                {
                    aInfo.EmpNo = Convert.ToInt32(dr["EmpNo"]);
                    aInfo.EMPID = dr["EMPID"].ToString();
                    aInfo.EmpName = dr["EmpName"].ToString();
                    aInfo.Gender = dr["Gender"].ToString();
                    aInfo.Designation = dr["Designation"].ToString();
                    aInfo.JoiningDate = dr["JoiningDate"].ToString();
                    aInfo.GradeName = dr["GradeName"].ToString();
                    aInfo.DepartmentName = dr["DepartmentName"].ToString();
                    aInfo.JobLocation = dr["JobLocationName"].ToString();
                    aInfo.SalaryLocation = dr["SalaryLocation"].ToString();
                    aInfo.EmpType = dr["EmpType"].ToString();
                    aInfo.CompanyName = dr["CompanyName"].ToString();
                    aInfo.OfficeEmail = dr["OfficeEmail"].ToString();
                    aInfo.PersonalMobile = dr["PersonalMobileNo"].ToString();
                    aInfo.OfficeMobile = dr["OfficeMobileNo"].ToString();
                    aInfo.Address = dr["PresentAddress"].ToString();
                    aInfo.BloodGroup = dr["BloodGroupName"].ToString();
                }

              return aInfo;

            }
            catch (Exception e)
            {
                throw e;
         
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }




        public EmployeeInfo _getEmployeeInfoByEmpId(string empId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empId", empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetEmpinfoByEmpId_AIMSAPI", aSqlParameterList);
                EmployeeInfo aInfo = new EmployeeInfo();
                while (dr.Read())
                {
                    aInfo.EmpNo = Convert.ToInt32(dr["EmpNo"]);
                    aInfo.EMPID = dr["EMPID"].ToString();
                    aInfo.EmpName = dr["EmpName"].ToString();
                    //aInfo.Gender = dr["Gender"].ToString();
                    aInfo.Designation = dr["Designation"].ToString();
                    //aInfo.JoiningDate = dr["JoiningDate"].ToString();
                    //aInfo.GradeName = dr["GradeName"].ToString();
                    //aInfo.DepartmentName = dr["DepartmentName"].ToString();
                    //aInfo.JobLocation = dr["JobLocationName"].ToString();
                    //aInfo.SalaryLocation = dr["SalaryLocation"].ToString();
                    //aInfo.EmpType = dr["EmpType"].ToString();
                    //aInfo.CompanyName = dr["CompanyName"].ToString();
                    //aInfo.OfficeEmail = dr["OfficeEmail"].ToString();
                    //aInfo.PersonalMobile = dr["PersonalMobileNo"].ToString();
                    //aInfo.OfficeMobile = dr["OfficeMobileNo"].ToString();
                    //aInfo.Address = dr["PresentAddress"].ToString();
                    //aInfo.BloodGroup = dr["BloodGroupName"].ToString();
                }

                return aInfo;

            }
            catch (Exception e)
            {
                throw e;

            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }





        public List<LeaveInformation> _getEmployeeLeaveList(string empId)
        {
            List<LeaveInformation> aInfo = new List<LeaveInformation>();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empId", empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetEmployeeLeaveList", aSqlParameterList);

                while (dr.Read())
                {
                    LeaveInformation aInformation = new LeaveInformation();
                    aInformation.LeaveId = Convert.ToInt32(dr["LeaveId"]);
                    aInformation.LeaveTypeId = Convert.ToInt32(dr["LeaveTypeId"]);
                    aInformation.LeaveType = dr["LeaveType"].ToString();
                    aInformation.StrLeaveDateRange = dr["StrLeaveDateRange"].ToString();
                    aInformation.Purpose = dr["Purpose"].ToString();
                    aInformation.AddressDuringLeave = dr["AddressDuringLeave"].ToString();
                    //aInformation.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aInformation.FromDateStr = dr["FromDate"].ToString();
                    aInformation.ToDateDateStr = dr["ToDate"].ToString();
                    aInformation.RequestToName = dr["FunctionReportingName"].ToString();
                    aInformation.AdminAuthName = dr["AdminReportingName"].ToString();
                    aInformation.LeaveAppliedCount =Convert.ToDecimal(dr["LeaveApplied"]);
                    aInformation.ResponsiblePersonDuringLeave = dr["ResponsiblePersonDuringLeave"].ToString();
                    aInformation.ApproveStatusStr = dr["IsApprove"].ToString();
                    aInformation.RequestStatusStr = dr["IsRequestApprove"].ToString();
                    aInformation.IsShortLeave = Convert.ToBoolean(dr["IsShortLeave"]);


                    aInfo.Add(aInformation);


                }


                return aInfo;
            }
            catch (Exception e)
            {
                LeaveInformation aInformation = new LeaveInformation();
                aInformation.IsError = true;


                aInfo.Add(aInformation);
                return aInfo;

            }

     
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }






        public LeaveInformation DeleteLeave(int leaveId)
        {
            LeaveInformation aLea = new LeaveInformation();


            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

         

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@leaveId", leaveId));
                aLea.IsSuccess = accessManager.UpdateData("sp_DeleteLeave", aSqlParameterList);
            }
            catch (Exception exception)
            {

                aLea.IsSuccess = false;
                aLea.IsError = true;
            }

 
            finally
            {
                accessManager.SqlConnectionClose();
            }

            return aLea;
        }



        public List<LeaveInformation> _getEmployeeLeaveListForApproval(string empId)
        {
            List<LeaveInformation> aInfo = new List<LeaveInformation>();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@empId", empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetEmployeeLeaveList_forApproval", aSqlParameterList);

                while (dr.Read())
                {
                    LeaveInformation aInformation = new LeaveInformation();
                    aInformation.LeaveId = Convert.ToInt32(dr["LeaveId"]);
                    aInformation.LeaveTypeId = Convert.ToInt32(dr["LeaveTypeId"]);
                    aInformation.LeaveType = dr["LeaveType"].ToString();
                    aInformation.StrLeaveDateRange = dr["StrLeaveDateRange"].ToString();
                    aInformation.Purpose = dr["Purpose"].ToString();
                    aInformation.AddressDuringLeave = dr["AddressDuringLeave"].ToString();
                    aInformation.FromDateStr = dr["FromDate"].ToString();
                    aInformation.ToDateDateStr = dr["ToDate"].ToString();
                    aInformation.RequestToName = dr["FunctionReportingName"].ToString();
                    aInformation.AdminAuthName = dr["AdminReportingName"].ToString();
                    aInformation.LeaveAppliedCount = Convert.ToDecimal(dr["LeaveApplied"]);
                    aInformation.ResponsiblePersonDuringLeave = dr["ResponsiblePersonDuringLeave"].ToString();
                    aInformation.ApproveStatusStr = dr["IsApprove"].ToString();
                    aInformation.RequestStatusStr = dr["IsRequestApprove"].ToString();
                    aInformation.EmpName = dr["EmpName"].ToString();
                    aInformation.IsShortLeave = Convert.ToBoolean(dr["IsShortLeave"]);


                    aInfo.Add(aInformation);


                }


                return aInfo;
            }
            catch (Exception e)
            {
                LeaveInformation aInformation = new LeaveInformation();
                aInformation.IsError = true;


                aInfo.Add(aInformation);
                return aInfo;

            }


            finally
            {
                accessManager.SqlConnectionClose();
            }
        }





        public LeaveInformation ApproveLeave(int leaveid, string stMessage, string approveType, int approveBy)
        {
            LeaveInformation aLea = new LeaveInformation();


            try
            {
                accessManager.SqlConnectionOpen(DataBase.AIMSAPPDB);

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@leaveid", leaveid));
                aSqlParameterList.Add(new SqlParameter("@stMessage", stMessage));
                aSqlParameterList.Add(new SqlParameter("@approveType", approveType));
                aSqlParameterList.Add(new SqlParameter("@approveBy", approveBy));
                aSqlParameterList.Add(new SqlParameter("@approveDate", DateTime.Now));
                aLea.IsSuccess = accessManager.UpdateData("sp_ApproveLeaveApplication", aSqlParameterList);
            }
            catch (Exception exception)
            {

                aLea.IsSuccess = false;
                aLea.IsError = true;
            }


            finally
            {
                accessManager.SqlConnectionClose();
            }

            return aLea;
        }



    }
}