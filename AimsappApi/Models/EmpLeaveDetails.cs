﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class EmpLeaveDetails
    {


        public int EmpNo { get; set; }
        public string Gender { get; set; }
        public string TCausal { get; set; }
        public string TMedical { get; set; }
        public string TAnnual { get; set; }
        public string ECausal { get; set; }
        public string EMedical { get; set; }
        public string EAnnual { get; set; }
        public string ACausal { get; set; }
        public string AMedical { get; set; }
        public string AAnnual { get; set; }


        public string BCausal { get; set; }
        public string BMedical { get; set; }
        public string BAnnual { get; set; }
        public string LWP { get; set; }
        public string Maternity { get; set; }

   





        public string FunctionReportingName { get; set; }
        public string FunctionReportingID { get; set; }
        public string AdminReportingName { get; set; }
        public string AdminReportingID { get; set; }
        public string ErrorMessage { get; set; }

        public bool IsError { get; set; }
    }
}