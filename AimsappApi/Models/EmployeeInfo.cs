﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class EmployeeInfo
    {

        public int EmpNo { get; set; }
        public string EMPID { get; set; }
        public string EmpName { get; set; }
        public string Gender { get; set; }
        public string Designation { get; set; }
        public string JoiningDate { get; set; }
        public string GradeName { get; set; }
        public string DepartmentName { get; set; }
        public string JobLocation { get; set; }
        public string SalaryLocation { get; set; }
        public string EmpType { get; set; }
        public string CompanyName { get; set; }
        public string OfficeEmail { get; set; }
        public string PersonalMobile { get; set; }
        public string OfficeMobile { get; set; }

        public string Address { get; set; }

        public string BloodGroup { get; set; }

    }
}