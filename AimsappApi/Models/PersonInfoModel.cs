﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class PersonInfoModel
    {
        public string PersonId { get; set; }
        public string PersonCode { get; set; }
        public string PersonName { get; set; }
        public string PersonAddress { get; set; }
        public string PersonMobileNo { get; set; }
        public string PersonTypeID { get; set; }
        public string PersonFamType { get; set; }
        public string PersonApproved { get; set; }
        public string PersonLat { get; set; }
        public string PersonLong { get; set; }

    }
}