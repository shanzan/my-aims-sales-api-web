﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class PaySlipModel
    {
        public int TotalAbsent { get; set; }
        public int Absenteeism { get; set; }
        public int Gross { get; set; }
        public int ActualGross { get; set; }
        public int Basic { get; set; }
        public int HouseRent { get; set; }
        public int Medical { get; set; }
        public int MobileAllowance { get; set; }
        public int OtherAllowance { get; set; }
        public int Tax { get; set; }
        public int TaxDeductionFromEmp { get; set; }
        public int TaxAdjustFromCompany { get; set; }
        public int OtherDeduction { get; set; }
        public int NetPayable { get; set; }
        public int ConveyanceAllowance { get; set; }
        public int SpecialAllowance { get; set; }
        public int FoodCharge { get; set; }
        public int TiffinAllowance { get; set; }
        public int Arrear { get; set; }
        public int AdvancedSalaryDeduction { get; set; }
        public int MotorCycleLoadDeduction { get; set; }
        public int ProvidentfundDeduction { get; set; }
        public int ProvidentFundFromCompany { get; set; }
        public int PFLoanDeduction { get; set; }
        public int TransportAllowance { get; set; }
        public int MobileBillDeduction { get; set; }
        public int HardshipAllowance { get; set; }
        public int OtherEarningAllowance { get; set; }
        public int FarmAllowance { get; set; }
        public int TechnicalAllowance { get; set; }
        public int UtilityDeduction { get; set; }
        public int AccommodationAllowance { get; set; }
        public int ConsultedAmount { get; set; }
        public int FoodAllowance { get; set; }
        public int AdjustmentDeduction { get; set; }
        public int HouseAllowanceDeduction { get; set; }
        public int SpecialBonus { get; set; }
        public string EmpType { get; set; }
        public string EmpName { get; set; }
        public string EMPID { get; set; }
        public string Designation { get; set; }
        public string PaySlipMonth { get; set; }
        public string YearNo { get; set; }
        public string JobLocationName { get; set; }



        public Boolean IsError { get; set; }

        public string ErrorMessage { get; set; }


        public int CalenderId { get; set; }


    }
}