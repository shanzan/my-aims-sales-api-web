﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class DailyDocRate
    {
        public string RateDate { get; set; }
        public string SAreaName { get; set; }
        public string RevisionTime { get; set; }
        public string Nwm { get; set; }
        public string Nwf { get; set; }
        public string Nbm { get; set; }
        public string Nbf { get; set; }
        public string Hc { get; set; }
        public string HbColor { get; set; }
        public string Ir { get; set; }
        public string RevisionNo { get; set; }
        public string ErrorMessage { get; set; }
        public bool ErrorStatus { get; set; }


    }
}