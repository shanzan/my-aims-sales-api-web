﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class LeaveInformation
    {



        public int EmpNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int LeaveTypeId { get; set; }
        public decimal LeaveAppliedCount { get; set; }
        public bool IsShortLeave { get; set; }
        public bool? IsApprove { get; set; }
        public string Purpose { get; set; }
        public string RequestTo { get; set; }
        public string RequestToName { get; set; }
        public string AddressDuringLeave { get; set; }
        public string ResponsiblePersonDuringLeave { get; set; }
        public string ResponsiblePersonDuringLeaveName { get; set; }
        public string AdminAuth { get; set; }
        public string AdminAuthName { get; set; }
        public string ReviewReason { get; set; }
        public string Designation { get; set; }
        public string DepartmentName { get; set; }
        public string LeaveType { get; set; }
        public string EmpName { get; set; }
        public string EMPID { get; set; }


        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }







        public bool IsError { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public int LeaveId { get; set; }
        public string StrLeaveDateRange { get; set; }
        public string IsRequestApprove { get; set; }
        public string FromDateStr { get; set; }
        public string ToDateDateStr { get; set; }
        public string ApproveStatusStr { get; set; }
        public string RequestStatusStr { get; set; }








    }
}