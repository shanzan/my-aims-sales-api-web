﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class AppVersionModal
    {
        public int AppVersionID { get; set; }
        public string AppVersionName { get; set; }
        public int AppVersionNumber { get; set; }

        public Boolean ErrorStatus { get; set; }
        public string ErrorMessage { get; set; }
    }
}