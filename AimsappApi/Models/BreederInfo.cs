﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class BreederInfo
    {


        public string BreederName { get; set; }
        public string BreederShortName { get; set; }
        public string ErrorMessage { get; set; }
        public bool ErrorStatus { get; set; }

    }
}