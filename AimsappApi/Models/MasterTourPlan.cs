﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class MasterTourPlan
    {
        public string TourPlanId { get; set; }
        public string TourPlanDetaisId { get; set; }
        public string TourTrackingNumber { get; set; }
        public string DealerOfFarms { get; set; }
        public string VisitingPlace { get; set; }
        public string TerritoryName { get; set; }
        public string IsVisit { get; set; }
        public string PlaceName { get; set; }
        public string FarmType { get; set; }
        public string Type { get; set; }
        public string DealerType { get; set; }
        public string DistanceCover { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TourDate { get; set; }
        public string AgentCode { get; set; }
        public string TourPlanTypeID { get; set; }
        public string ComDealerCode { get; set; }
        public string SubDealerCode { get; set; }
        public string IsFarmer { get; set; }
        public string FarmerNo { get; set; }

        public string FarmerID { get; set; }
        public string DealerId { get; set; }
        public string SubDealerId { get; set; }
        public string ComDealerId { get; set; }
        public string PointLat { get; set; }
        public string PointLong { get; set; }

    }
}