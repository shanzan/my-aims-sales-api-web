﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class LeaveApplyNotifyReqInfo
    {


        public string OfficeMail { get; set; }
        public string OfficeMobile { get; set; }
        public string EMPID { get; set; }
        public string EmpName { get; set; }
        public string Designation { get; set; }

        public bool IsError { get; set; }
    }
}