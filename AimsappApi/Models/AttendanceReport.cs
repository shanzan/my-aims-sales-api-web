﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class AttendanceReport
    {

        public  int EmpNo { get; set; }

        public  string EMPID { get; set; }
        public  string AttDate { get; set; }
      public  string DayName { get; set; }
        public  string InTime { get; set; }
        public  string OutTime { get; set; }
        public  string DutyDuration { get; set; }
        public  string Status { get; set; }
        public  string ShiftStart { get; set; }
        public  string ShiftEnd { get; set; }
        public  string Remarks { get; set; }
        public  string ActualAttDate { get; set; }




    }
}