﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class PsProductionData
    {

        public string ProductionDate { get; set; }
        public string FilterName { get; set; }
        public string House { get; set; }
        public string FlockAge { get; set; }
        public string Mm { get; set; }
        public string Mc { get; set; }
        public string Mk { get; set; }
        public string Fm { get; set; }
        public string Fc { get; set; }
        public string Fk { get; set; }

        public string ProdStd { get; set; }
        public string ActPercent { get; set; }
        public string Pe { get; set; }
        public string He { get; set; }
        public string Hhp { get; set; }
        public string Type { get; set; }

        public Boolean ErrorStatus { get; set; }
        public string ErrorMessage { get; set; }
        public string FlockNo { get; set; }
    }
}