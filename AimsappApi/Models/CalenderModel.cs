﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AimsappApi.Models
{
    public class CalenderModel
    {

        public int CalendarId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime Todate { get; set; }
        public int YearNo { get; set; }
        public string MonthName { get; set; }
        public int MonthNo { get; set; }
        public string cDate { get; set; }

        public Boolean IsError { get; set; }
        public Boolean IsCurrentMonth { get; set; }

        public string ErrorMessage { get; set; }

    }
}