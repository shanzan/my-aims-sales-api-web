﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AimsappApi.DAL;
using AimsappApi.Models;

namespace AimsappApi.Controllers
{
    public class BreederExtensionController : ApiController
    {

        PsDataDAL _psDataDal = new PsDataDAL();


        [ResponseType(typeof(BreederInfo))]
        [HttpGet]
        public IHttpActionResult GetBreederInfo()
        {
            return Json(_psDataDal._getBreederActiveInfo());
        }



    }
}
