﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AimsappApi.DAL;

namespace AimsappApi.Controllers
{
    public class PsDataController : ApiController
    {

        PsDataDAL _psDataDal = new PsDataDAL();
        [HttpGet]
        public IHttpActionResult GetPsData(DateTime productionDate,string typeB,string empId)
        {
            return Json(_psDataDal._getPsProductionData(productionDate, typeB, empId));
        }





    }
}
