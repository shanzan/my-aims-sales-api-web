﻿using AimsappApi.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AimsappApi.Controllers
{
    public class TourDetailsController : ApiController
    {
        TourDetailsDAL tourdet = new TourDetailsDAL();

        [HttpGet]
        public IHttpActionResult GetTourPlanDetails(DateTime selecteddate, DateTime endDate, string empId)
        {
            return Json(tourdet.get_tourDetailsByEmpAndDate(selecteddate, endDate, empId));
        }
        [HttpGet]
        public IHttpActionResult GetFilterFarmType(DateTime selecteddate, DateTime endDate, string empId, String layer, String broiler, String cattle, String sonali, String fish, String colorbird)
        {
            return Json(tourdet.get_filterFarmtypes(selecteddate, endDate, empId, layer, broiler, cattle, sonali, fish, colorbird));
        }

        [HttpGet]
        public IHttpActionResult SavePlaceDetails(string tourdetailsId, string address, double latitude, double longitude, double distance)
        {
            return Json(tourdet.SavePlaceDetailsInfo(tourdetailsId, address, latitude, longitude, distance));
        }
    }
}
