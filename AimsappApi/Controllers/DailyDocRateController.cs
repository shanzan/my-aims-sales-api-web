﻿using AimsappApi.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AimsappApi.Controllers
{
    public class DailyDocRateController : ApiController
    {
        DailyDocRateDAL docrateDal = new DailyDocRateDAL();


        [HttpGet]
        public IHttpActionResult GetDailyDocRate(DateTime currentDate,string empId)
        {
            return Json(docrateDal._getDailyDocRate(currentDate,empId));
        }


    }
}
