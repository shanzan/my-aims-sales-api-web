﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AimsappApi.DAL;
using AimsappApi.Models;

namespace AimsappApi.Controllers
{
    public class EmployeeController : ApiController
    {
        LoginDAL loginDAL = new LoginDAL();
        EmployeeDAL empDAL = new EmployeeDAL();

        /// <summary>
        /// Login request
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult LoginRequest(string username, string password)
        {
            return Json(loginDAL.LoginCheck(username, password));

        }

        /// <summary>
        /// ALl Employee info by empno
        /// </summary>
        /// <param name="empno"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetEmpinfoByEmpNo(int empno)
        {
            return Json(loginDAL._getEmployeeInfoByEmpNo(empno));
        }


        /// <summary>
        /// Getting Employee Attendance information
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="empId"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetEmployeeAttendance(DateTime fromDate, DateTime toDate,string empId)
        {
            return Json(empDAL._getEmployeeAttendance(fromDate, toDate, empId));

        }

        /// <summary>
        /// getting calender data for payslip
        /// </summary>
        /// <param name="calender"></param>
        /// <returns></returns>

        [HttpGet]
        public IHttpActionResult GetCalenderData(string calender)
        {
            return Json(empDAL.GetCalendarData());

        }


        /// <summary>
        /// getting payslip data
        /// </summary>
        /// <param name="calenderId"></param>
        /// <param name="empId"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetPaySlipData(int calenderId,string empId)
        {
            return Json(empDAL.GetPaySlipData(calenderId, empId));

        }

        /// <summary>
        /// getting employee leave info
        /// </summary>
        /// <param name="EmpNoLeave"></param>
        /// <returns></returns>

        [HttpGet]
        public IHttpActionResult GetLeaveInformation(int EmpNoLeave)
        {


            return Json(empDAL.GetEmpLeaveDetails(EmpNoLeave));
        }


        /// <summary>
        /// saving leave
        /// </summary>
        /// <param name="aLeave"></param>
        /// <returns></returns>
        [HttpPost]

        public IHttpActionResult SaveLeaveApplication(LeaveInformation aLeave)
        {

            if (aLeave.LeaveId > 0)
            {
                return Json(empDAL.UpdateLeave(aLeave));
            }
            else
            {
                return Json(empDAL.SaveLeave(aLeave));
            }
       
        }
        /// <summary>
        /// specific employee
        /// </summary>
        /// <param name="empid"></param>
        /// <returns></returns>

        [HttpGet]
        public IHttpActionResult GetSpecificEmployeeByEmpId(string empid)
        {
            return Json(loginDAL._getEmployeeInfoByEmpId(empid));
        }

        /// <summary>
        /// Employee leave list by id
        /// </summary>
        /// <param name="empidLeavList"></param>
        /// <returns></returns>


        [HttpGet]
        public IHttpActionResult GetEmployeeLeaveList(string empidLeavList)
        {
            return Json(loginDAL._getEmployeeLeaveList(empidLeavList));
        }


        /// <summary>
        /// Deleting leave
        /// </summary>
        /// <param name="leaveId"></param>
        /// <returns></returns>

        [HttpGet]
        public IHttpActionResult DeleteLeave(int leaveId)
        {
            return Json(loginDAL.DeleteLeave(leaveId));
        }

        /// <summary>
        /// This method will generate list for leave approval authority. Need only empid to get list
        /// </summary>
        /// <param name="empIdLeaveApproval"> This is just for Functional Authority ID</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult LeaveApprovalList(string empIdLeaveApproval)
        {
            return Json(loginDAL._getEmployeeLeaveListForApproval(empIdLeaveApproval));
        }



        /// <summary>
        /// Approving Leave 
        /// </summary>
        /// <param name="leaveid"></param>
        /// <param name="stMessage"></param>
        /// <param name="approveType"></param>
        /// <param name="approveBy"></param>
        /// <returns></returns>

        [HttpGet]
        public IHttpActionResult ApproveLeave(int leaveid ,string stMessage,string approveType,int approveBy)
        {
            return Json(loginDAL.ApproveLeave(leaveid, stMessage, approveType, approveBy));
        }






























    }
}
