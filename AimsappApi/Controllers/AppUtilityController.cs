﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AimsappApi.DAL;
using AimsappApi.Models;

namespace AimsappApi.Controllers
{
    public class AppUtilityController : ApiController
    {

        AppUtilityDAL _mDAL = new AppUtilityDAL();

        /// <summary>
        /// This will get App version information that is active
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(AppVersionModal))]
        [HttpGet]
        public IHttpActionResult GetAppVersionInfo()
        {
            return Json(_mDAL._getAppVersionInfo());
        }
    }
}
