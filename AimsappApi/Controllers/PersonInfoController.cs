﻿using AimsappApi.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AimsappApi.Controllers
{
    public class PersonInfoController : ApiController
    {
        PersonInfoDAL myperson = new PersonInfoDAL();

       
        [HttpGet]
        [Route("api/{PersonInfo}/DealersInfo")]
        [ActionName("GetDealers")]
        public IHttpActionResult GetDealerInfo(string empID)
        {
            return Json(myperson.get_DealerInformation(empID));
        }

    
        [HttpGet]
        [Route("api/{PersonInfo}/FarmersInfo")]
        [ActionName("GetFarmers")]
        public IHttpActionResult GetFarmerInformation(string empID)
        {
            return Json(myperson.get_FarmerInformation(empID));
        }

        [HttpGet]
        [Route("api/{PersonInfo}/SubDealerInfo")]
        [ActionName("GetSubDealer")]
        public IHttpActionResult GetSubDealerInformation(string empID)
        {
            return Json(myperson.get_subDealerInformation(empID));
        }

        [HttpGet]
        public IHttpActionResult SaveLocationInfo(string personId, string personCode, int personType, double platitude, double plongitude, string pFarmtype, string empID)
        {
            return Json(myperson.SaveLocationInformation(personId, personCode, personType, platitude, plongitude, pFarmtype,empID));
        }
    }
}
